# Encrypt secrets

```
cat secret.yml | kubeseal --format yaml --cert sealed-secrets.pem > sealed-secret.yml
```
