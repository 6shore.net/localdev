# Installation

We use https://kind.sigs.k8s.io/ instead of minikube to run tests on local laptop.

```
$ go install sigs.k8s.io/kind@v0.17.0
go: downloading sigs.k8s.io/kind v0.17.0
go: downloading github.com/spf13/cobra v1.4.0
go: downloading github.com/alessio/shellescape v1.4.1
go: downloading github.com/pelletier/go-toml v1.9.4
go: downloading github.com/evanphx/json-patch/v5 v5.6.0
go: downloading github.com/google/safetext v0.0.0-20220905092116-b49f7bc46da2
go install sigs.k8s.io/kind@v0.17.0

$ kind create cluster
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.25.3) 🖼
⢎⡰ Preparing nodes 📦  ^R
 ✓ Preparing nodes 📦
 ✓ Writing configuration 📜
 ✓ Starting control-plane 🕹️
 ✓ Installing CNI 🔌
 ✓ Installing StorageClass 💾
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Not sure what to do next? 😅  Check out https://kind.sigs.k8s.io/docs/user/quick-start/
```

Kubernetes should be running

```
$ kubectl get node
NAME                 STATUS   ROLES           AGE   VERSION
kind-control-plane   Ready    control-plane   56s   v1.25.3
```

Now install argocd with apps

```
argocd$ ./install.sh
"argo" has been added to your repositories
Release "argocd" does not exist. Installing it now.
NAME: argocd
LAST DEPLOYED: Wed Nov  2 12:44:20 2022
NAMESPACE: argocd
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
In order to access the server UI you have the following options:

1. kubectl port-forward service/argocd-server -n argocd 8080:443

    and then open the browser on http://localhost:8080 and accept the certificate

2. enable ingress in the values file `server.ingress.enabled` and either
      - Add the annotation for ssl passthrough: https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#option-1-ssl-passthrough
      - Set the `configs.params."server.insecure"` in the values file and terminate SSL at your ingress: https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#option-2-multiple-ingress-objects-and-hosts


After reaching the UI the first time you can login with username: admin and the random password generated during the installation. You can find the password by running:

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

(You should delete the initial secret afterwards as suggested by the Getting Started Guide: https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli)
deployment.apps/argocd-server condition met
ArcoCD password: vyPYQuVaBYqI6osD
```

```
/localdev/apps$ kubectl apply -f apps.yml
application.argoproj.io/apps created
```