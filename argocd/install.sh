#!/bin/bash
# create sealed secret key
gpg --decrypt sealed-secrets-key.yml.asc 2> /dev/null | kubectl apply -f -


# bootstrap argocd
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install --create-namespace --values values.yml --namespace argocd argocd argo/argo-cd

# Wait for agrocd to start
kubectl wait --for=condition=available --timeout=180s deployment/argocd-server -n argocd
echo -n "ArcoCD username admin and password: "
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
echo